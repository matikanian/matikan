from django import forms
from sabte_ahval import models
from .models import Author, Publisher, Category


class SearchForm(forms.Form):
    """ search title in book list form """
    search_title = forms.CharField(
        label="نام کتاب",
        max_length=200,
        widget=forms.TextInput(attrs={
            'class': 'form-control centre',
            'placeholder': 'نام کتاب'
        }),
        required=False
    )
    author = forms.ModelChoiceField(
        queryset=Author.objects.all(), empty_label="همه نویسندگان", required=False)
    publisher = forms.ModelChoiceField(
        queryset=Publisher.objects.all(), empty_label="همه ناشران", required=False)
    categories = forms.ModelMultipleChoiceField(
        queryset=Category.objects.all(), widget= forms.CheckboxSelectMultiple, required=False)


class CommentForm(forms.ModelForm):
    action_url = ""
    method = "post"
    class Meta:
        model = models.Comment
        fields = ["text", "point"]
        widgets = {
            'text': forms.Textarea(attrs={
                'class': 'form-control',
                'rows': 4,
                'placeholder': 'نقد'
            }),
            'point': forms.RadioSelect
        }
