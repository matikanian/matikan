""" views for ketabgard """
from django.views import generic, View
from django.urls import reverse
from .forms import SearchForm, CommentForm
from .models import Book
from sabte_ahval import models as sabte_ahval_models
from django.http import HttpResponse,HttpResponseRedirect
from hitcount.views import HitCountDetailView

class book_profile(HitCountDetailView):
    """ showing book profile """
    model = Book
    template_name = 'ketabgard/profile.html'
    count_hit = True

    def get_context_data(self, **kwargs):
        """ set contex vars and adding seach form to list """
        context = super().get_context_data(**kwargs)
        # getting all comments for current book
        context["comments"] = sabte_ahval_models.Comment.objects.filter(
            book=self.object)
        #get number of 0 points
        context["0_points"] = sabte_ahval_models.Comment.objects.filter(
            book=self.object).filter(point=0).count()
        #get number of 1 points
        context["1_points"] = sabte_ahval_models.Comment.objects.filter(
            book=self.object).filter(point=1).count()
        #get number of 2 points
        context["2_points"] = sabte_ahval_models.Comment.objects.filter(
            book=self.object).filter(point=2).count()   
        # getting self comment if exists
        context["self_comment"] = context["comments"].filter(
            user_id=self.request.user.id)

        if self.object.already_read.filter(pk=self.request.user.id).exists():
            context["already_read"] = True
        else:
            context["already_read"]= False

        if self.object.reading.filter(pk=self.request.user.id).exists():
            context["reading"] = True
        else:
            context["reading"]= False

        if self.object.to_read.filter(pk=self.request.user.id).exists():
            context["to_read"] = True
        else:
            context["to_read"]= False

        # sending comment form
        context["form"] = CommentForm()
        return context

    def post(self, request, *args, **kwargs):
        """ saving comment if anything sent with post """
        new_comment = sabte_ahval_models.Comment(
            book=self.get_object(),
            user=request.user,
            text=request.POST.get('text'),
            point=request.POST.get('point'),
        )
        new_comment.save()
        self.object = self.get_object()  # isnt there any more clear way?
        # rendering normal book profile
        return super().render_to_response(self.get_context_data())

class assign_book_to_already_read_list(View):
    def post(self, request, *args, **kwargs):
        book_pk = self.request.POST.get("book_pk")
        Book.objects.get(pk=book_pk).already_read.add(self.request.user)
        return HttpResponseRedirect(reverse("ketabgard:book_profile",kwargs={'pk':book_pk}))
    
class unassign_book_to_already_read_list(View):
    def post(self, request, *args, **kwargs):
        book_pk = self.request.POST.get("book_pk")
        Book.objects.get(pk=book_pk).already_read.remove(self.request.user)
        return HttpResponseRedirect(reverse("ketabgard:book_profile",kwargs={'pk':book_pk}))

class assign_book_to_reading_list(View):
    def post(self, request, *args, **kwargs):
        book_pk = self.request.POST.get("book_pk")
        Book.objects.get(pk=book_pk).reading.add(self.request.user)
        return HttpResponseRedirect(reverse("ketabgard:book_profile",kwargs={'pk':book_pk}))
    
class unassign_book_to_reading_list(View):
    def post(self, request, *args, **kwargs):
        book_pk = self.request.POST.get("book_pk")
        Book.objects.get(pk=book_pk).reading.remove(self.request.user)
        return HttpResponseRedirect(reverse("ketabgard:book_profile",kwargs={'pk':book_pk}))

class assign_book_to_to_read_list(View):
    def post(self, request, *args, **kwargs):
        book_pk = self.request.POST.get("book_pk")
        Book.objects.get(pk=book_pk).to_read.add(self.request.user)
        return HttpResponseRedirect(reverse("ketabgard:book_profile",kwargs={'pk':book_pk}))
    
class unassign_book_to_to_read_list(View):
    def post(self, request, *args, **kwargs):
        book_pk = self.request.POST.get("book_pk")
        Book.objects.get(pk=book_pk).to_read.remove(self.request.user)
        return HttpResponseRedirect(reverse("ketabgard:book_profile",kwargs={'pk':book_pk}))

class book_list(generic.ListView):
    """ list books also support search with GET """
    model = Book
    context_object_name = "books"
    template_name = 'ketabgard/list.html'

    def get_context_data(self, **kwargs):
        """ set contex vars and adding seach form to list """
        context = super().get_context_data(**kwargs)
        context["form"] = SearchForm()
        if self.request.GET.get("search_title"):
            context["form"].fields['search_title'].widget.attrs["value"] = self.request.GET.get("search_title")
        if self.request.GET.get("author"):
            context["form"].fields['author'].initial = self.request.GET.get("author")
        if self.request.GET.get("publisher"):
            context["form"].fields['publisher'].initial = self.request.GET.get("publisher")
        if self.request.GET.getlist("categories"):
            context["form"].fields['categories'].initial = self.request.GET.getlist("categories")
        return context

    def get_queryset(self):
        """ prepare books and filter them if there is GET on search_title """
        result = super().get_queryset()
        search_title = self.request.GET.get("search_title")
        author = self.request.GET.get("author")
        publisher = self.request.GET.get("publisher")
        cats = self.request.GET.getlist("categories")
        if search_title:
            for word in search_title.split():
                result = result.filter(title__icontains=word)
        if author:
            result = result.filter(author=author)
        if publisher:
            result = result.filter(publisher=publisher)
        if cats:
            for cat in cats:
                result = result.filter(category__in=cat)
        return result

class book_top(generic.ListView):
    """ show top 6 books """
    model = Book
    context_object_name = "books"
    template_name = 'ketabgard/top.html'

    def get_queryset(self):
        result = super().get_queryset()
        result = result.order_by("-hit_count_generic__hits")[:6]
        return result
