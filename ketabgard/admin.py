from django.contrib import admin
from django import forms

from .models import *
from sabte_ahval import models as sabte_ahval_models

class CommentAdmin(admin.ModelAdmin):
    list_filter = ["book", "user"]
    search_fields = ["book", "user"]
admin.site.register(sabte_ahval_models.Comment, CommentAdmin)

class BookForm(forms.ModelForm):
    """
    adding new book or modify it for admin
    """
    class Meta:
        model = Book
        fields = "__all__"

    def clean_title(self):
        """
        check if title is unique
        """
        title = self.cleaned_data['title']
        # exclude self pk when edditing
        same = Book.objects.filter(title=title).exclude(pk=self.instance.pk)
        if same.exists():
            raise forms.ValidationError(
                "there is a book with this informations. pk=" + str(same.get().id))
        return title

class CommentInBooks(admin.TabularInline):
    model = sabte_ahval_models.Comment
    extra = 2

class BookAdmin(admin.ModelAdmin):
    """ admin model for books
    just set default form to BookForm
    """
    form = BookForm
    inlines = [CommentInBooks]

admin.site.register(Book, BookAdmin)


class AuthorForm(forms.ModelForm):
    """
    adding new author or modify it for admin
    """
    class Meta:
        model = Author
        fields = "__all__"

    def clean_name(self):
        """
        check if author_name is unique
        """
        author_name = self.cleaned_data['name']
        # exclude self pk when edditing
        same = Author.objects.filter(name=author_name).exclude(pk=self.instance.pk)
        if same.exists():
            raise forms.ValidationError(
                "there is an author with this informations. pk=" + str(same.get().id))
        return author_name

class AuthorAdmin(admin.ModelAdmin):
    """ admin model for authors
    just set default form to authorForm
    """
    form = AuthorForm

admin.site.register(Author, AuthorAdmin)

class CategoryForm(forms.ModelForm):
    """
    adding new category or modify it for admin
    """
    class Meta:
        model = Category
        fields = "__all__"

    def clean_name(self):
        """
        check if Category is unique
        """
        category = self.cleaned_data['name']
        # exclude self pk when edditing
        same = Category.objects.filter(name=category).exclude(pk=self.instance.pk)
        if same.exists():
            raise forms.ValidationError(
                "there is a category with this informations. pk=" + str(same.get().id))
        return category

class CategoryAdmin(admin.ModelAdmin):
    """ admin model for categories
        just set default form to CategoryForm
    """
    form = CategoryForm

admin.site.register(Category, CategoryAdmin)


class TranslatorForm(forms.ModelForm):
    """
    adding new translator or modify it for admin
    """
    class Meta:
        model = Translator
        fields = "__all__"

    def clean_name(self):
        """
        check if Translator is unique
        """
        translator = self.cleaned_data['name']
        # exclude self pk when edditing
        same = Translator.objects.filter(name=translator).exclude(pk=self.instance.pk)
        if same.exists():
            raise forms.ValidationError(
                "there is a translator with this informations. pk=" + str(same.get().id))
        return translator

class TranslatorAdmin(admin.ModelAdmin):
    """ admin model for translators
        just set default form to PublisherForm
    """
    form = TranslatorForm

admin.site.register(Translator, TranslatorAdmin)


class PublisherForm(forms.ModelForm):
    """
    adding new publisher or modify it for admin
    """
    class Meta:
        model = Publisher
        fields = "__all__"

    def clean_name(self):
        """
        check if Publisher is unique
        """
        publisher = self.cleaned_data['name']
        # exclude self pk when edditing
        same = Publisher.objects.filter(name=publisher).exclude(pk=self.instance.pk)
        if same.exists():
            raise forms.ValidationError(
                "there is a publisher with this informations. pk=" + str(same.get().id))
        return publisher

class PublisherAdmin(admin.ModelAdmin):
    """ admin model for publishers
        just set default form to PublisherForm
    """
    form = PublisherForm

admin.site.register(Publisher, PublisherAdmin)