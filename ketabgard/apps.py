from django.apps import AppConfig


class KetabgardConfig(AppConfig):
    name = 'ketabgard'
