from django.db import models
from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericRelation

from hitcount.models import HitCount, HitCountMixin

class Author(models.Model):
    name = models.CharField(max_length=1024, default="UNKNOWN")
    def __str__(self):
        return self.name


class Translator(models.Model):
    name = models.CharField(max_length=1024, default="UNKNOWN")

class Publisher(models.Model):
    name = models.CharField(max_length=1024, default="UNKNOWN")
    def __str__(self):
        return self.name

class Category(models.Model):
    name = models.CharField(max_length=1024, default="UNKNOWN")
    def __str__(self):
        return self.name

class Book(models.Model):
    title = models.CharField(max_length=1024)
    picture_path = models.CharField(max_length=2048, default="")
    description = models.CharField(max_length=4096, null=True)
    author = models.ForeignKey(Author, on_delete=models.SET_DEFAULT, default=None, null=True)
    translator = models.ForeignKey(Translator, on_delete=models.SET_DEFAULT, default=None, null=True)
    publisher = models.ForeignKey(Publisher, on_delete=models.SET_DEFAULT, default=None, null=True)
    category = models.ManyToManyField(Category)
    already_read = models.ManyToManyField(User, related_name="already_read")
    reading = models.ManyToManyField(User, related_name="reading")
    to_read = models.ManyToManyField(User, related_name="to_read")
    hit_count_generic = GenericRelation(
        HitCount, object_id_field='object_pk',
        related_query_name='hit_count_generic_relation')

    def __str__(self):
        return self.title
