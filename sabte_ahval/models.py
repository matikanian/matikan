from django.db import models
from django.contrib.auth.models import User
from ketabgard import models as ketabgard_models

class Comment(models.Model):
    text = models.CharField(max_length=1024)
    point = models.SmallIntegerField(choices=[(1,"⭐️"),(2,"⭐️⭐️"),(3,"⭐️⭐️⭐️")], blank=False, default=2)
    book = models.ForeignKey(ketabgard_models.Book, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    
    def __str__(self):
        return self.user.get_username()+" on "+ self.book.title + " : " + self.text + " ⭐️" + str(self.point)

