from django.test import TestCase, Client

from .models import *
from .admin import BookForm, AuthorForm, CategoryForm, TranslatorForm, PublisherForm


class default_database():
    ok_author = Author(name="erfan th. author")
    author_list = [ok_author]

    ok_translator = Translator(name="erfan th. translator")
    translator_list = [ok_translator]

    ok_publisher = Publisher(name="erfan th. publisher")
    publisher_list = [ok_publisher]

    ok_category = Category(name="erfan th. category")
    category_list = [ok_category]

    ok_book1 = Book(
        title="new book",
        picture_path='http://google.com/logo.png',
        description='very effective description',
        author=ok_author.pk,
        translator=ok_translator.pk,
        publisher=ok_publisher.pk
    )
    ok_book1_categories = [ok_category]
    ok_book2 = Book(
        title="ketabe jadid va khafan",
        picture_path='http://google.com/logo.png',
        description='another tozih',
        author=ok_author.pk,
        translator=ok_translator.pk,
        publisher=ok_publisher.pk
    )
    ok_book2_categories = [ok_category]

    book_list = [
        (ok_book1, ok_book1_categories),
        (ok_book2, ok_book2_categories),
    ]

    @staticmethod
    def commit():
        for author in default_database.author_list:
            author.save()

        for translator in default_database.translator_list:
            translator.save()

        for publisher in default_database.publisher_list:
            publisher.save()

        for category in default_database.category_list:
            category.save()

        for book in default_database.book_list:
            book[0].save()
            book[0].category.set(book[1])


class ketabgard_test(TestCase):

    def setUp(self):
        default_database.commit()
        self.client = Client()

    def test_all_book(self):
        response = self.client.get('/book/list/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['books']), 2)

    def test_search_one_word_valid(self):
        response = self.client.get('/book/list/?search_title=new')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.context['books'][0], default_database.ok_book1)

    def test_search_tow_word_valid(self):
        response = self.client.get('/book/list/?search_title=jadid ketab')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.context['books'][0], default_database.ok_book2)

    def test_search_none(self):
        response = self.client.get('/book/list/?search_title=none')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['books']), 0)


class admin_add_book_panel_test(TestCase):

    def setUp(self):
        default_database.commit()

    def test_add_valid_book(self):
        form = BookForm(data={
            'title': 'erfan is here men!',
            'picture_path': 'http://google.com/logo.png',
            'description': 'very effective description',
            'author': Author.objects.first().pk,
            'translator': Translator.objects.first().pk,
            'publisher': Publisher.objects.first().pk,
            'category': [Category.objects.first().pk]
        })
        form.save()

    def test_add_cuncurrent_book(self):
        form = BookForm(data={
            'title': default_database.ok_book1.title,
            'picture_path': 'http://google.com/logo.png',
            'description': 'very effective description',
            'author': Author.objects.first().pk,
            'translator': Translator.objects.first().pk,
            'publisher': Publisher.objects.first().pk,
            'category': [Category.objects.first().pk]
        })
        self.assertFalse(form.is_valid())

    def test_add_cuncurrent_author(self):
        form = AuthorForm(data={
            'name': default_database.ok_author.name,
        })
        self.assertFalse(form.is_valid())

    def test_add_cuncurrent_category(self):
        form = CategoryForm(data={
            'name': default_database.ok_category.name,
        })
        self.assertFalse(form.is_valid())

    def test_add_cuncurrent_translator(self):
        form = TranslatorForm(data={
            'name': default_database.ok_translator.name,
        })
        self.assertFalse(form.is_valid())

    def test_add_cuncurrent_publisher(self):
        form = PublisherForm(data={
            'name': default_database.ok_publisher.name,
        })
        self.assertFalse(form.is_valid())
