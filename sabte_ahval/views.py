from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect, HttpResponse
from django import forms
from .forms import UserRegistrationForm
from ketabgard import models as ketabgard_models
from django.urls import reverse

def home(request):
    return render(request, 'sabte_ahval/index.html')
def register(request):
    if request.method == 'POST':
        form = UserRegistrationForm(request.POST)
        if form.is_valid():
            userObj = form.cleaned_data
            username = userObj['username']
            email =  userObj['email']
            password =  userObj['password']
            if not (User.objects.filter(username=username).exists() or User.objects.filter(email=email).exists()):
                User.objects.create_user(username, email, password)
                user = authenticate(username = username, password = password)
                login(request, user)
                return HttpResponseRedirect(reverse('sabte_ahval:user_profile', args=(user.pk,)))
            else:
                return HttpResponse('Looks like a username with that email already exists')
    else:
        form = UserRegistrationForm()
    return render(request, 'registration/register.html', {'form' : form})

    

from django.views import generic

class already_read_list(generic.ListView):
    model = ketabgard_models.Book
    context_object_name = "books"
    template_name = 'sabte_ahval/already_read.html'

    def get_queryset(self):
        books=ketabgard_models.Book.objects.filter(already_read=self.request.user)
        return books
    
class reading_list(generic.ListView):
    model = ketabgard_models.Book
    context_object_name = "books"
    template_name = 'sabte_ahval/reading.html'

    def get_queryset(self):
        books=ketabgard_models.Book.objects.filter(reading=self.request.user)
        return books

class to_read_list(generic.ListView):
    model = ketabgard_models.Book
    context_object_name = "books"
    template_name = 'sabte_ahval/to_read.html'

    def get_queryset(self):
        books=ketabgard_models.Book.objects.filter(to_read=self.request.user)
        return books

class user_profile(generic.DetailView):
    """ showing user profile """
    model = User 
    template_name = 'sabte-ahval/profile.html'

   