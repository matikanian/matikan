from django import forms
class UserRegistrationForm(forms.Form):
    username = forms.CharField(
        required = True,
        label = 'نام کاربری',
        max_length = 32,
         widget=forms.TextInput(attrs={
            'class': 'form-control centre'
         
        })
    )
    email = forms.CharField(
        required = True,
        label = 'ایمیل',
        max_length = 64,
        widget=forms.TextInput(attrs={
            'class': 'form-control centre'
            
        })
    )
    password = forms.CharField(
        required = True,
        label = 'رمز عبور',
        max_length = 32,
        widget = forms.PasswordInput(attrs={
            'class': 'form-control'})
    )

    