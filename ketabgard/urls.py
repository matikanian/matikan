# Create your views here.
from django.urls import path

from . import views

app_name = 'ketabgard'
urlpatterns = [
    path('<int:pk>/', views.book_profile.as_view(), name='book_profile'),
    path('list/', views.book_list.as_view(), name='book_list'),
    path('top/', views.book_top.as_view(), name='book_top'),
    
    path('list/add1/', views.assign_book_to_already_read_list.as_view(), name='add_to_already_read_list'),
    path('list/remove1/', views.unassign_book_to_already_read_list.as_view(), name='remove_from_already_read_list'),

    path('list/add2/', views.assign_book_to_reading_list.as_view(), name='add_to_reading_list'),
    path('list/remove2/', views.unassign_book_to_reading_list.as_view(), name='remove_from_reading_list'),

    path('list/add3/', views.assign_book_to_to_read_list.as_view(), name='add_to_to_read_list'),
    path('list/remove3/', views.unassign_book_to_to_read_list.as_view(), name='remove_from_to_read_list'),

]
