from django.shortcuts import render

# Create your views here.
from django.urls import path
from .views import home, register

from . import views
from django.contrib.auth import views as auth_views
app_name = 'sabte_ahval'
urlpatterns = [
    path('', home),
    path('login/', auth_views.login),
    path('register/', register),
    path('<int:pk>/already_read/', views.already_read_list.as_view(), name='already_read'),
    path('<int:pk>/reading/', views.reading_list.as_view(), name='reading'),
    path('<int:pk>/to_read/', views.to_read_list.as_view(), name='to_read'),
    path('<int:pk>/', views.user_profile.as_view(), name='user_profile'),
    
]
